use poll_promise::Promise;

use super::{deck_info::DeckInfo, SerializableState};

pub struct LoadingApp {
    pub state: SerializableState,
    pub info: Promise<DeckInfo>
}

impl Default for LoadingApp {
    fn default() -> Self {
        Self::new(Default::default())
    }
}

impl LoadingApp {
    pub fn new(state: SerializableState) -> Self {
        Self {
            state: state.clone(),
            info: super::spawn(async move {
                let mut info = DeckInfo::new(state);
                info.load();
                info
            })
        }
    }
}

impl eframe::App for LoadingApp {
    /// Called each time the UI needs repainting, which may be many times per second.
    /// Put your widgets into a `SidePanel`, `TopPanel`, `CentralPanel`, `Window` or `Area`.
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        self.state.top_bottom_menu(ctx, _frame);

        egui::CentralPanel::default().show(ctx, |ui| {
            ui.centered_and_justified(|ui| {
                ui.horizontal_centered(|ui| {
                    ui.label("loading");
                    ui.spinner()
                });
            });
        });
        self.state.theme_edit(ctx);
    }

    /// Called by the frame work to save state before shutdown.
    fn save(&mut self, storage: &mut dyn eframe::Storage) {
        eframe::set_value(storage, eframe::APP_KEY, &self.state);
    }
}
