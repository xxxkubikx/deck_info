use sr_libs::{cff, card_rendering, utils};
use card_rendering::cff_impl::{CARD_ELEMENT_TYPES, CardRenderingDataCff, DrawableElementTextureType, DrawableElementType};
use rusttype::Font;
use utils::card_templates::CardTemplate;

pub async fn render_art_work(card: CardTemplate, definition: &xmltree::Element,
                             data: &'static cff::GameDataCffFile, texts: &'static cff::LanguageCffFile) -> egui::ColorImage
{
    for d in definition.children.iter() {
        let d = d.as_element().unwrap();
        let element_type = &d.attributes["type"];
        if "kType_Artwork" != element_type {
            continue;
        }
        let (_, draw) = CARD_ELEMENT_TYPES
            .iter()
            .find(|(n, _)| n == element_type)
            .unwrap();
        let data = CardRenderingDataCff { card, data, texts, upgrade: 3, charges: 3 };
        if let DrawableElementType::Texture(texture_type) = draw(&data) {
            match download_texture(texture_type, d).await {
                None => {}
                Some(texture) => {
                    //return egui_extras::image::RetainedImage::from_color_image(
                    //    format!("{}", card.base_id()), to_image(texture));
                    return to_image(texture);
                }
            }
        }
        break;
    }

    let mut image = image::RgbaImage::new(50, 50);
    fill(&mut image, image::Rgba([255, 127, 255, 255]));
    //egui_extras::image::RetainedImage::from_color_image(format!("{}", card.base_id()), to_image(image))
    to_image(image)
}

pub async fn render(card: CardTemplate, definition: &xmltree::Element,
                    data: &'static cff::GameDataCffFile, texts: &'static cff::LanguageCffFile, font: &Font<'_>) -> egui::ColorImage
{
    let data = CardRenderingDataCff::<'static> { card, data, texts, charges: 3, upgrade: 3 };

    let h: u32 = definition.attributes["height"].parse().unwrap();
    let w: u32 = definition.attributes["width"].parse().unwrap();
    let mut image = image::RgbaImage::new(w, h);
    fill(&mut image, image::Rgba([0, 0, 0, 0]));
    for d in definition.children.iter() {
        let d = d.as_element().unwrap();
        let x: u32 = d.attributes["x"].parse().unwrap();
        let y: u32 = d.attributes["y"].parse().unwrap();
        let h: u32 = d.attributes["height"].parse().unwrap();
        let w: u32 = d.attributes["width"].parse().unwrap();
        let color = color(d);
        let element_type = &d.attributes["type"];
        if "kType_Description" == element_type {
            continue // TODO read all description
        }
        let (_, draw) = CARD_ELEMENT_TYPES
            .iter()
            .find(|(n, _)| n == element_type)
            .unwrap();
        match draw(&data) {
            DrawableElementType::None => {}
            DrawableElementType::Rectangle => {
                //println!("rect {} x:{} y:{} w:{} h:{} color:0x{:x}", element_type, x, y, w, h, color);
                let mut overlay = image::RgbaImage::new(w, h);
                fill(&mut overlay, color);
                mix(&mut image, &overlay, x, y);
            }
            DrawableElementType::Texture(texture_type) => draw_texture(&mut image, texture_type, d).await,
            DrawableElementType::Text(text) => draw_text(&mut image, &text, d, font),
        }
    }

    //egui_extras::image::RetainedImage::from_color_image(format!("{}", card.base_id()), to_image(image))
    to_image(image)
}

async fn download_texture(texture_type: DrawableElementTextureType<'_>, d: &xmltree::Element) -> Option<image::RgbaImage> {
    let texture = match texture_type {
        DrawableElementTextureType::CardTexture(texture) => {
            texture
        }
        DrawableElementTextureType::ElementTexture => {
            &d.attributes["texture"]
        }
    };

    if let Ok(url) = reqwest::Url::parse("https://jakub.bandola.cz/static/")
        .unwrap()
        .join(&texture.to_lowercase()) {
        //dbg!(&texture, &url);

        if let Ok(response) = reqwest::get(url).await {
            if response.status().is_success() {
                let h: u32 = d.attributes["height"].parse().unwrap();
                let w: u32 = d.attributes["width"].parse().unwrap();
                let color = color(d);
                let bytes = response.bytes().await.unwrap();

                let cursor = std::io::Cursor::new(bytes);
                let format = if texture.ends_with(".tga") {
                    image::ImageFormat::Tga
                } else if texture.ends_with(".dds") {
                    image::ImageFormat::Dds
                } else {
                    image::ImageFormat::Png
                };
                let img = image::io::Reader::with_format(cursor.clone(), format).decode()
                    // some SR replaced files have intentionally wrong extension to trick the game into using them
                                                                                .unwrap_or_else(|_| image::io::Reader::with_format(cursor, image::ImageFormat::Png).decode().unwrap());
                let img = img.resize(w, h, image::imageops::FilterType::Triangle);
                let mut img = img.into_rgba8();
                if u32::from_le_bytes(color.0) != u32::MAX {
                    let [r, g, b, a] = color.0;
                    for p in img.pixels_mut() {
                        if p.0[3] > 0 {
                            let r = ((r as f32 / 256. * p.0[0] as f32 / 256.) * 256.) as u8;
                            let g = ((g as f32 / 256. * p.0[1] as f32 / 256.) * 256.) as u8;
                            let b = ((b as f32 / 256. * p.0[2] as f32 / 256.) * 256.) as u8;
                            let a = ((a as f32 / 256. * p.0[3] as f32 / 256.) * 256.) as u8;
                            *p = image::Rgba([r, g, b, a]);
                        }
                    }
                }
                return Some(img);
            } else {
                println!("Response: {} for: {}", response.status(), texture);
            }
        } else {
            println!("Can not get response for: {}", texture);
        }
    } else {
        println!("Bad path: {}", texture);
    }
    None
}

async fn draw_texture(image: &mut image::RgbaImage, texture_type: DrawableElementTextureType<'_>, d: &xmltree::Element) {
    match download_texture(texture_type, d).await {
        None => {}
        Some(texture) => {
            let x: u32 = d.attributes["x"].parse().unwrap();
            let y: u32 = d.attributes["y"].parse().unwrap();
            mix(image, &texture, x, y);
        }
    }
}

fn draw_text(image: &mut image::RgbaImage, text: &str, d: &xmltree::Element, font: &Font<'_>) {
    let x: u32 = d.attributes["x"].parse().unwrap();
    let y: u32 = d.attributes["y"].parse().unwrap();
    let h: u32 = d.attributes["height"].parse().unwrap();
    let w: u32 = d.attributes["width"].parse().unwrap();
    //let _color = color(d);
    let h_align = d.attributes.get("h_align").map(String::as_str).unwrap_or("center");
    let v_align = d.attributes.get("v_align").map(String::as_str).unwrap_or("center");
    //println!("text {} x:{} y:{} w:{} h:{} color:0x{:x} align: {}/{} test: {}", element_type, x, y, w, h, color, h_align, v_align, text);
    let mut overlay = image::RgbaImage::new(w, h);
    //fill(&mut overlay, image::Rgba([128,128,0,255]));
    let scale = rusttype::Scale::uniform(font.scale_for_pixel_height(35000.));
    const X: i32 = 5;
    const Y: i32 = 10;
    let mut min_x = u32::MAX;
    let mut max_x = 0;
    let mut min_y = u32::MAX;
    let mut max_y = 0;
    //dbg!(scale);
    let line_height = font.v_metrics(scale).line_gap + font.v_metrics(scale).ascent + font.v_metrics(scale).descent;
    for (i, line) in text.lines().enumerate() {
        let y = 0. + line_height * i as f32;
        for g in font.layout(line, scale, rusttype::Point { x: 0., y }) {
            //dbg!(g.pixel_bounding_box());
            //dbg!(&p);
            if g.id().0 == b'\n' as u16 {
                continue
            }
            let bb = g.pixel_bounding_box().unwrap_or_default().min;
            g.draw(|x, y, v| {
                //println!("{} {} {}", x, y, v);
                let x = x + (bb.x + X) as u32;
                let y = y + (bb.y + Y) as u32;
                if x < w && y < h {
                    overlay.put_pixel(x, y, image::Rgba([255, 255, 255, (255. * v) as u8]));
                    if x < min_x {
                        min_x = x;
                    }
                    if x > max_x {
                        max_x = x;
                    }
                    if y < min_y {
                        min_y = y;
                    }
                    if y > max_y {
                        max_y = y;
                    }
                }
            });
        }
    }
    let _xb = x;
    let x =
        match h_align {
            "left" => x,
            "center" => {
                let rw = max_x - min_x;
                (w - rw) / 2 + x
            }
            _ => x
        }.saturating_sub(X as u32 * 4 / 5);
    let _yb = y;
    let y =
        match v_align {
            "bottom" => y + h - (max_y - min_y),
            "center" => {
                let rh = max_y - min_y;
                (h - rh) / 2 + y
            }
            _ => y
        }.saturating_sub(Y as u32 / 3);
    //println!("x: {}->{} y: {}->{}", _xb, x, _yb, y);
    mix(image, &overlay, x, y);
}

fn color(d: &xmltree::Element) -> image::Rgba<u8> {
    let color = d.attributes.get("color")
                 .map(|c| u32::from_str_radix(&c[2..], 16).unwrap())
                 .unwrap_or_default();

    let [b, g, r, a] = color.to_le_bytes();
    image::Rgba([r, g, b, a])
}

fn mix(target: &mut image::RgbaImage, overlay: &image::RgbaImage, x: u32, y: u32) {
    image::imageops::overlay(target, overlay, x as i64, y as i64)
}

fn to_image(img: image::RgbaImage) -> egui::ColorImage {
    let size = [img.width() as usize, img.height() as usize];
    egui::ColorImage::from_rgba_unmultiplied(size, img.as_flat_samples().as_slice())
}

fn fill(image: &mut image::RgbaImage, color: image::Rgba<u8>) {
    for y in 0..image.height() {
        for x in 0..image.width() {
            image.put_pixel(x, y, color)
        }
    }
}
