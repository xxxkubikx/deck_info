use std::collections::btree_map::Entry;
use std::collections::BTreeMap;

use eframe::Storage;
use egui::{ColorImage, Id, Response, Sense, Vec2, Widget};
use egui_extras::RetainedImage;
use sr_libs::{cff, filter, deck_codes};
use filter::{CardFilter, FakeAllCardsOwned};
use poll_promise::Promise;
use sr_libs::utils::card_templates::{CardTemplate, IntoEnumIterator};

use crate::app::card_renderer;

use super::SerializableState;

const MAX_DECK_SIZE: usize = 20;

pub struct DeckInfo {
    encode_deck: String,
    encode_deck_long: String,
    encode_deck_json: String,
    images: Images,
    all_cards_filter_str: String,
    all_cards_filter: CardFilter<'static>,
    state: SerializableState,
}

struct Images {
    game_data: &'static cff::GameDataCffFile,
    text_data: &'static cff::LanguageCffFile,
    image_template: &'static xmltree::Element,
    image_cache: BTreeMap<CardTemplate, RetainedImage>,
    image_cache_bin: BTreeMap<CardTemplate, Promise<ColorImage>>,
    artwork_image_cache: BTreeMap<CardTemplate, RetainedImage>,
    artwork_image_cache_bin: BTreeMap<CardTemplate, Promise<ColorImage>>,
    font: &'static rusttype::Font<'static>,
    two_rows: bool,
    only_artwork: bool,
}

impl Default for Images {
    fn default() -> Self {
        let font_data: &[u8] = include_bytes!("bf_data/tahoma.ttf");
        let owned_font_data: Vec<u8> = font_data.to_vec();
        let from_owned_font: rusttype::Font<'static> = rusttype::Font::try_from_vec(owned_font_data).unwrap();
        let game_data = cff::GameDataCffFile::from_bytes(include_bytes!("bf_data/gamedata.cff")).unwrap();
        let en = cff::LanguageCffFile::from_bytes(include_bytes!("bf_data/en.cff"), None).unwrap();
        filter::prepare::prepare_texts_action(&game_data, [&en].iter().copied());
        filter::card_info_from_game_data::initialize_card_info_helper(&game_data);
        Self {
            game_data: Box::leak(Box::new(game_data)),
            text_data: Box::leak(Box::new(en)),
            image_template:
            Box::leak(Box::new(xmltree::Element::parse(&include_bytes!("bf_data/carddefinitions.xml")[..])
                .unwrap().children.iter()
                .filter(|n| n.as_element().map(|e| e.attributes["type"] == "kCard_Default_179x250").unwrap_or_default())
                .next().unwrap().as_element().unwrap().clone())),
            image_cache: BTreeMap::new(),
            image_cache_bin: BTreeMap::new(),
            artwork_image_cache: BTreeMap::new(),
            artwork_image_cache_bin: BTreeMap::new(),
            font: Box::leak(Box::new(from_owned_font)),
            two_rows: false,
            only_artwork: false,
        }
    }
}

impl DeckInfo {
    pub fn new(state: SerializableState) -> Self {
        Self {
            encode_deck: "".to_string(),
            encode_deck_long: "".to_string(),
            encode_deck_json: "".to_string(),
            images: Images::default(),
            all_cards_filter_str: "promo=true".to_string(),
            all_cards_filter: CardFilter::All,
            state,
        }
    }

    pub fn load(&mut self) {
        self.all_cards_filter = filter::parse_expr(
            &self.all_cards_filter_str,
            &BTreeMap::default(),
            filter::prepare::helper()
        );
        if self.state.cache_version < 1 {
            self.state.cache_version = 1;
            fs::clear_cache();
        }
        self.images.load();
        self.encode();
    }

    fn encode(&mut self) {
        self.encode_deck =
            deck_codes::encode_base64_short_deck_from_iter(
                MAX_DECK_SIZE,
                self.state.encode_cards.iter()
                    .filter(|&&ct| ct != CardTemplate::NotACard)
                    .map(|&ct| ct.into()));
        self.encode_deck_long =
            deck_codes::encode_base64_deck_from_iter(
                MAX_DECK_SIZE,
                self.state.encode_cards.iter()
                    .filter(|&&ct| ct != CardTemplate::NotACard)
                    .map(|&ct| ct.into()));

        let a = self.state.encode_cards.iter().map(|ct| serde_json::json!(ct.base_id())).collect();
        self.encode_deck_json = serde_json::to_string(&serde_json::Value::Array(a)).unwrap();
    }
}

impl eframe::App for DeckInfo {
    /// Called each time the UI needs repainting, which may be many times per second.
    /// Put your widgets into a `SidePanel`, `TopPanel`, `CentralPanel`, `Window` or `Area`.
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        self.images.only_artwork = self.state.only_artwork;
        self.images.two_rows = self.state.two_rows;
        self.state.top_bottom_menu(ctx, _frame);

        egui::CentralPanel::default().show(ctx, |ui| {
            egui::ScrollArea::vertical().show(ui, |ui| {
                self.deck_to_code(ui);
                self.all_cards(ui);
            });
        });

        self.state.theme_edit(ctx);
    }

    /// Called by the frame work to save state before shutdown.
    fn save(&mut self, storage: &mut dyn Storage) {
        eframe::set_value(storage, eframe::APP_KEY, &self.state);
    }
}

impl DeckInfo {
    fn deck_to_code(&mut self, ui: &mut egui::Ui) {
        ui.collapsing("combo boxes", |ui| {
            ui.vertical(|ui| {
                let mut changed = false;
                for i in 0..MAX_DECK_SIZE {
                    let r = egui::ComboBox::new(i, format!("{}", self.state.encode_cards[i].base_id()))
                        .width(200.0)
                        .selected_text(format!("{}", self.state.encode_cards[i].name()))
                        .show_ui(ui, |ui| {
                            let mut changed_inner = false;
                            for ct in CardTemplate::iter() {
                                changed_inner |= ui.selectable_value(
                                    &mut self.state.encode_cards[i], ct, ct.name())
                                                   .clicked();
                            }
                            changed_inner
                        });
                    changed |= r.inner == Some(true);
                    if let Some(ct) = drop_target(ui, r.response) {
                        changed = true;
                        self.state.encode_cards[i] = ct;
                    }
                }
                if changed {
                    self.encode();
                }
            });
        });
        if egui::TextEdit::singleline(&mut self.encode_deck)
            .desired_width(ui.available_width())
            .ui(ui)
            .changed() {
            match deck_codes::decode_deck(&self.encode_deck) {
                Ok(deck) => {
                    for i in 0..MAX_DECK_SIZE {
                        self.state.encode_cards[i] = CardTemplate::NotACard;
                    }
                    for card in deck {
                        self.state.encode_cards[card.position as usize] = card.card;
                    }
                    self.encode();
                }
                Err(_) => {}
            }
        }
        if egui::TextEdit::singleline(&mut self.encode_deck_long)
            .desired_width(ui.available_width())
            .ui(ui)
            .changed() {
            match deck_codes::decode_deck(&self.encode_deck_long) {
                Ok(deck) => {
                    for i in 0..MAX_DECK_SIZE {
                        self.state.encode_cards[i] = CardTemplate::NotACard;
                    }
                    for card in deck {
                        self.state.encode_cards[card.position as usize] = card.card;
                    }
                    self.encode();
                }
                Err(_) => {}
            }
        }
        if egui::TextEdit::singleline(&mut self.encode_deck_json)
            .desired_width(ui.available_width())
            .ui(ui)
            .changed() {
            match deck_codes::decode_deck(&self.encode_deck_json) {
                Ok(deck) => {
                    for i in 0..MAX_DECK_SIZE {
                        self.state.encode_cards[i] = CardTemplate::NotACard;
                    }
                    for card in deck {
                        self.state.encode_cards[card.position as usize] = card.card;
                    }
                    self.encode();
                }
                Err(_) => {}
            }
        }
        if let Some((i, ct)) = self.images.cards_row(self.state.encode_cards.iter().copied(), ui) {
            self.state.encode_cards[i] = ct;
            self.encode();
        }
    }

    fn all_cards(&mut self, ui: &mut egui::Ui) {
        ui.separator();
        if egui::TextEdit::singleline(&mut self.all_cards_filter_str)
            .desired_width(ui.available_width())
            .ui(ui).changed() {
            self.all_cards_filter = filter::parse_expr(
                &self.all_cards_filter_str,
                &BTreeMap::default(),
                filter::prepare::helper()
            );
        }
        ui.allocate_space(Vec2::new(0., 5.));
        egui::ScrollArea::vertical().show(ui, |ui: &mut egui::Ui| {
            ui.horizontal_wrapped(|ui: &mut egui::Ui| {
                for c in CardTemplate::iter()
                    .filter(|c| self.all_cards_filter.apply(c, &FakeAllCardsOwned {}) && c.name() != "Test Placeholder")
                {
                    let c: CardTemplate = c;
                    let id = Id::new(CARDS_DRAG).with(c.base_id());
                    drag_source(ui, id, |ui| { self.images.img_for(c, 200., ui) });
                    if ui.available_height() <= 0. {
                        break
                    }
                }
            });
        });
    }
}

const CARDS_DRAG: &str = "CARDS_DRAG";

fn drop_target(ui: &mut egui::Ui, response: Response) -> Option<CardTemplate> {
    if let Some(pointer_pos) = ui.ctx().pointer_interact_pos() {
        if ui.memory(|mem| mem.is_anything_being_dragged()) {
            if response.rect.contains(pointer_pos) {
                if ui.input(|input| input.pointer.any_released()) {
                    for c in CardTemplate::iter().skip(1) {
                        let c: CardTemplate = c;
                        let id = Id::new(CARDS_DRAG).with(c.base_id());
                        if ui.memory(|mem| mem.is_being_dragged(id)) {
                            return Some(c)
                        }
                    }
                }
            }
        }
    }
    None
}

fn drag_source(ui: &mut egui::Ui, id: Id, body: impl FnOnce(&mut egui::Ui) -> Response) {
    let is_being_dragged = ui.memory(|mem| mem.is_being_dragged(id));

    if !is_being_dragged {
        let response = body(ui);
        //let response = ui.scope(body).response;

        // Check for drags:
        let response = ui.interact(response.rect, id, Sense::drag());
        if response.hovered() {
            ui.ctx().set_cursor_icon(egui::CursorIcon::Grab);
        }
    } else {
        ui.ctx().set_cursor_icon(egui::CursorIcon::Grabbing);

        // Paint the body to a new layer:
        let layer_id = egui::LayerId::new(egui::Order::Tooltip, id);
        let response = ui.with_layer_id(layer_id, body).response;

        // Now we move the visuals of the body to where the mouse is.
        // Normally you need to decide a location for a widget first,
        // because otherwise that widget cannot interact with the mouse.
        // However, a dragged component cannot be interacted with anyway
        // (anything with `Order::Tooltip` always gets an empty [`Response`])
        // So this is fine!

        if let Some(pointer_pos) = ui.ctx().pointer_interact_pos() {
            let delta = pointer_pos - response.rect.center();
            ui.ctx().translate_layer(layer_id, delta);
        }
    }
}

impl Images {
    fn load(&mut self) {
        fs::load(&mut self.artwork_image_cache_bin, &mut self.image_cache_bin);
    }
    pub fn img_for(&mut self, card: CardTemplate, max_width: f32, ui: &mut egui::Ui) -> Response {
        if self.only_artwork {
            match self.artwork_image_cache.entry(card) {
                Entry::Vacant(e) => {
                    let p = self.artwork_image_cache_bin.entry(card).or_insert_with(|| {
                        if let Some(img) = fs::load_artwork(card) {
                            Promise::from_ready(img)
                        } else {
                            let future =
                                card_renderer::render_art_work(card, self.image_template, self.game_data, &self.text_data);
                            super::spawn(future)
                        }
                    });
                    if let Some(img) = p.ready() {
                        fs::save_artwork(card, &img);
                        let img = RetainedImage::from_color_image(format!("{}", card.base_id()), img.clone());
                        e.insert(img);
                        ui.ctx().request_repaint();
                    }
                    return ui.allocate_response(Vec2::new(10., 10.), Sense::focusable_noninteractive())
                }
                Entry::Occupied(e) => {
                    e.get().show_max_size(ui, Vec2::new(max_width, f32::MAX))
                }
            }
        } else {
            match self.image_cache.entry(card) {
                Entry::Vacant(e) => {
                    let p = self.image_cache_bin.entry(card).or_insert_with(|| {
                        if let Some(img) = fs::load_card(card) {
                            Promise::from_ready(img)
                        } else {
                            let future =
                                card_renderer::render(card, &self.image_template, &self.game_data, &self.text_data, &self.font);
                            super::spawn(future)
                        }
                    });
                    if let Some(img) = p.ready() {
                        fs::save_card(card, &img);
                        let img = RetainedImage::from_color_image(format!("{}", card.base_id()), img.clone());
                        e.insert(img);
                        ui.ctx().request_repaint();
                    }
                    return ui.allocate_response(Vec2::new(10., 10.), Sense::focusable_noninteractive())
                }
                Entry::Occupied(e) => {
                    e.get().show_max_size(ui, Vec2::new(max_width, f32::MAX))
                }
            }
        }
    }
    fn cards_row<I: Iterator<Item = CardTemplate> + Clone>(&mut self, iter: I, ui: &mut egui::Ui) -> Option<(usize, CardTemplate)> {
        let mut ret = None;
        if self.two_rows {
            let max_width = ui.available_width() / MAX_DECK_SIZE as f32 * 2. - ui.spacing().item_spacing.x;
            ui.horizontal(|ui| {
                for (i, card) in iter.clone().take(MAX_DECK_SIZE / 2).enumerate() {
                    let r = self.img_for(card, max_width, ui);
                    ret = ret.xor(drop_target(ui, r).map(|c| (i, c)))
                }
            });
            ui.horizontal(|ui| {
                for (i, card) in iter.skip(MAX_DECK_SIZE / 2).enumerate() {
                    let r = self.img_for(card, max_width, ui);
                    ret = ret.xor(drop_target(ui, r).map(|c| (i + 10, c)))
                }
            });
        } else {
            ui.horizontal(|ui| {
                let max_width = ui.available_width() / MAX_DECK_SIZE as f32 - ui.spacing().item_spacing.x;
                for (i, card) in iter.enumerate() {
                    let r = self.img_for(card, max_width, ui);
                    ret = ret.xor(drop_target(ui, r).map(|c| (i, c)))
                }
            });
        }
        ret
    }
}

#[cfg(not(target_arch = "wasm32"))]
mod fs {
    use std::fs::DirEntry;
    use std::io::{Read, Write};
    use std::path::PathBuf;

    use poll_promise::Promise;
    use sr_libs::utils::card_templates::CardTemplate;

    pub fn clear_cache() {
        if let Some(proj_dirs) = directories_next::ProjectDirs::from("", "", crate::APP_NAME) {
            let data_dir: PathBuf = proj_dirs.data_dir().to_path_buf();
            let artwork = data_dir.join("artwork");
            let _ = std::fs::remove_dir_all(artwork);
            let cards = data_dir.join("cards");
            let _ = std::fs::remove_dir_all(cards);
        }
    }

    pub fn load_artwork(_card: CardTemplate) -> Option<egui::ColorImage> {
        None
    }

    pub fn load_card(_card: CardTemplate) -> Option<egui::ColorImage> {
        None
    }

    pub fn load(artwork_image_cache_bin: &mut std::collections::BTreeMap<CardTemplate, Promise<egui::ColorImage>>,
                image_cache_bin: &mut std::collections::BTreeMap<CardTemplate, Promise<egui::ColorImage>>)
    {
        if let Some(proj_dirs) = directories_next::ProjectDirs::from("", "", crate::APP_NAME) {
            let data_dir: PathBuf = proj_dirs.data_dir().to_path_buf();
            let artwork = data_dir.join("artwork");
            if let Ok(_) = std::fs::create_dir_all(&artwork) {
                if let Ok(entries) = artwork.read_dir() {
                    for (ct, img) in entries.filter_map(read_file) {
                        artwork_image_cache_bin.insert(ct, Promise::from_ready(img));
                    }
                }
            }
            let cards = data_dir.join("cards");
            if let Ok(_) = std::fs::create_dir_all(&cards) {
                if let Ok(entries) = cards.read_dir() {
                    for (ct, img) in entries.filter_map(read_file) {
                        image_cache_bin.insert(ct, Promise::from_ready(img));
                    }
                }
            }
        }
    }

    fn read_file(e: std::io::Result<DirEntry>) -> Option<(CardTemplate, egui::ColorImage)> {
        let file = e.ok()?;
        let path = file.path();
        let name = path.file_name()?.to_str()?;
        let mut parts = name.split("_");
        let id = parts.next()?;
        let x = parts.next()?;
        let y = parts.next()?;
        let id = id.parse().ok()?;
        let x = x.parse().ok()?;
        let y = y.parse().ok()?;
        let mut file = std::fs::File::open(path).ok()?;
        let mut buf = vec![];
        let _ = file.read_to_end(&mut buf).ok()?;
        let img = egui::ColorImage::from_rgba_unmultiplied([x, y], &buf);
        Some((CardTemplate::from_u16(id), img))
    }

    pub fn save_artwork(card: CardTemplate, artwork: &egui::ColorImage) {
        save_image(card, artwork, "artwork");
    }

    pub fn save_card(card: CardTemplate, artwork: &egui::ColorImage) {
        save_image(card, artwork, "cards");
    }

    fn save_image(card: CardTemplate, img: &egui::ColorImage, base: &str) {
        if let Some(proj_dirs) = directories_next::ProjectDirs::from("", "", crate::APP_NAME) {
            let data_dir: PathBuf = proj_dirs.data_dir().to_path_buf();
            let path = data_dir.join(base);
            if let Ok(_) = std::fs::create_dir_all(&path) {
                let path = path.join(format!("{}_{}_{}", card.base_id(), img.size[0], img.size[1]));
                let data: Vec<u8> = img.pixels.iter().flat_map(egui::Color32::to_srgba_unmultiplied).collect();
                let _ = write_img(path, &data);
            }
        }
    }

    fn write_img(path: PathBuf, data: &[u8]) -> std::io::Result<()> {
        if !path.exists() {
            std::fs::File::create(&path)?.write_all(data)?;
        }
        Ok(())
    }
}

#[cfg(target_arch = "wasm32")]
mod fs {
    use poll_promise::Promise;
    use utils::card_templates::CardTemplate;

    pub fn clear_cache() {
        use strum::IntoEnumIterator;
        for base in ["artwork", "cards"] {
            for card in CardTemplate::iter() {
                let name = format!("{}.{}", base, card.base_id());
                if eframe::web::local_storage_get(&name).is_some() {
                    eframe::web::local_storage_set(&name, "cleared");
                }
            }
        }
    }

    pub fn load(_artwork_image_cache_bin: &mut std::collections::BTreeMap<CardTemplate, Promise<eframe::egui::ColorImage>>,
                _image_cache_bin: &mut std::collections::BTreeMap<CardTemplate, Promise<eframe::egui::ColorImage>>)
    {}

    pub fn load_artwork(card: CardTemplate) -> Option<eframe::egui::ColorImage> {
        load_image(card, "artwork")
    }

    pub fn load_card(card: CardTemplate) -> Option<eframe::egui::ColorImage> {
        load_image(card, "cards")
    }

    fn load_image(card: CardTemplate, base: &str) -> Option<eframe::egui::ColorImage> {
        let name = format!("{}.{}", base, card.base_id());
        let size = eframe::web::local_storage_get(&name)?;
        let mut size = size.split(" ");
        let x = size.next()?.parse().ok()?;
        let y = size.next()?.parse().ok()?;
        let data_name = format!("{}.{}.data", base, card.base_id());
        let img_data = eframe::web::local_storage_get(&data_name)?;
        let buf = base64::decode(&img_data).ok()?;
        let image = eframe::egui::ColorImage::from_rgba_unmultiplied([x, y], &buf);
        Some(image)
    }

    pub fn save_artwork(card: CardTemplate, artwork: &eframe::egui::ColorImage) {
        save_image(card, artwork, "artwork");
    }

    pub fn save_card(card: CardTemplate, artwork: &eframe::egui::ColorImage) {
        save_image(card, artwork, "cards");
    }

    fn save_image(card: CardTemplate, img: &eframe::egui::ColorImage, base: &str) {
        let name = format!("{}.{}", base, card.base_id());
        let data_name = format!("{}.{}.data", base, card.base_id());
        eframe::web::local_storage_set(&name, &format!("{} {}", img.size[0], img.size[1]));
        let data: Vec<u8> = img.pixels.iter().flat_map(eframe::egui::Color32::to_srgba_unmultiplied).collect();
        write_img(&data_name, &data)
    }

    fn write_img(key: &str, data: &[u8]) {
        eframe::web::local_storage_set(key, &base64::encode(data))
    }
}
