#![warn(clippy::all, rust_2018_idioms)]

pub const APP_NAME : &'static str = "Deck preview";

mod app;
pub use app::TemplateApp;
