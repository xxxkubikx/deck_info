use eframe::{Frame, Storage};
use eframe::epaint::Color32;
use egui::Context;
use poll_promise::Promise;
use sr_libs::utils::card_templates::CardTemplate;

use crate::app::deck_info::DeckInfo;
use crate::app::loading::LoadingApp;

mod loading;
mod deck_info;
mod card_renderer;

/// We derive Deserialize/Serialize so we can persist app state on shutdown.
pub enum TemplateApp {
    Loading(LoadingApp),
    Run(DeckInfo)
}

#[derive(serde::Deserialize, serde::Serialize)]
#[serde(default)]
#[derive(Default, Clone)]
#[derive(Debug)]
pub struct SerializableState {
    editing_theme: bool,
    theme: egui::Style,
    encode_cards: [CardTemplate; 20],
    all_cards_filter_str: String,
    two_rows: bool,
    only_artwork: bool,
    cache_version: u64,
}

impl SerializableState {
    fn top_bottom_menu(&mut self, ctx: &Context, _frame: &mut Frame) {
        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
            // The top panel is often a good place for a menu bar:
            egui::menu::bar(ui, |ui: &mut egui::Ui| {
                #[cfg(not(target_arch = "wasm32"))] // no File->Quit on web pages!
                ui.menu_button("File", |ui| {
                    if ui.button("Quit").clicked() {
                        _frame.close();
                    }
                });
                #[cfg(target_arch = "wasm32")]
                ui.menu_button("Download", |ui| {
                    ui.hyperlink_to("Windows EXE", "https://jakub.bandola.cz/wa/deck_info/deck_info.exe");
                });
                ui.checkbox(&mut self.two_rows, "two rows");
                ui.checkbox(&mut self.only_artwork, "only artwork");
                if ui.button("edit theme").clicked() {
                    self.editing_theme = true;
                }
            });
        });

        egui::TopBottomPanel::bottom("bottom").show(ctx, |ui| {
            ui.horizontal(|ui| {
                ui.hyperlink_to("Report a bug with tag @Kubik", "https://discord.com/channels/173414671678832640/801219974177751050");
                ui.hyperlink_to("Or directly at gitlab (where you can also check the code", "https://gitlab.com/xxxkubikx/deck_info");
                // TODO flush image cache button
            });
        });
    }

    fn theme_edit(&mut self, ctx: &Context) {
        egui::Window::new("theme").open(&mut self.editing_theme).show(ctx, |ui: &mut egui::Ui| {
            if ui.button("dark").clicked() {
                self.theme.visuals = egui::Visuals::dark();
                ctx.set_style(self.theme.clone());
            }
            if ui.button("dark + forum background").clicked() {
                self.theme.visuals = egui::Visuals::dark();
                self.theme.visuals.widgets.noninteractive.bg_fill = Color32::from_rgb(24, 24, 24);
                ctx.set_style(self.theme.clone());
            }
            if ui.button("light").clicked() {
                self.theme.visuals = egui::Visuals::light();
                ctx.set_style(self.theme.clone());
            }
            ui.collapsing("custom", |ui: &mut egui::Ui| {
                self.theme.ui(ui);
                ctx.set_style(self.theme.clone());
            });
        });
    }
}

impl Default for TemplateApp {
    fn default() -> Self {
        Self::Loading(Default::default())
    }
}

#[cfg(target_arch = "wasm32")]
fn spawn<T: Send>(future: impl std::future::Future<Output = T> + 'static) -> Promise<T> {
    Promise::spawn_async(future)
}

#[cfg(not(target_arch = "wasm32"))]
fn spawn<T: Send>(future: impl std::future::Future<Output = T> + Send + 'static) -> Promise<T> {
    Promise::spawn_thread("", || {
        let rt = tokio::runtime::Builder::new_current_thread().enable_all().build().unwrap();
        rt.block_on(future)
    })
}

impl TemplateApp {
    /// Called once before the first frame.
    pub fn new(cc: &eframe::CreationContext<'_>) -> Self {
        // This is also where you can customize the look and feel of egui using
        // `cc.egui_ctx.set_visuals` and `cc.egui_ctx.set_fonts`.

        // Load previous app state (if any).
        // Note that you must enable the `persistence` feature for this to work.
        if let Some(storage) = cc.storage {
            if let Some(state) = eframe::get_value::<SerializableState>(storage, eframe::APP_KEY) {
                cc.egui_ctx.set_style(state.theme.clone());
                return Self::Loading(LoadingApp::new(state));
            }
        }

        Self::Loading(LoadingApp::new(Default::default()))
    }
}

impl eframe::App for TemplateApp {
    fn update(&mut self, ctx: &Context, frame: &mut Frame) {
        match self {
            TemplateApp::Loading(loading) => {
                loading.update(ctx, frame);

                if loading.info.poll().is_ready() {
                    let (_, mut info) = Promise::new();
                    std::mem::swap(&mut info, &mut loading.info);
                    if let Ok(info) = info.try_take() {
                        *self = TemplateApp::Run(info);
                    }
                }
            }
            TemplateApp::Run(app) => {
                app.update(ctx, frame)
            }
        }
    }

    fn save(&mut self, storage: &mut dyn Storage) {
        match self {
            TemplateApp::Loading(loading) => {
                loading.save(storage)
            }
            TemplateApp::Run(app) => {
                app.save(storage)
            }
        }
    }
}
